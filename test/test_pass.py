#!/usr/bin/python

import os
import unittest

import userdir_ldap.ldap


class TestPassword(unittest.TestCase):
    def test_genpass(self):
        passwords = [userdir_ldap.ldap.GenPass() for i in range(10)]
        self.assertEqual(len(set(passwords)), 10)
        self.assertEqual(set(len(p) for p in passwords), {15})
        self.assertEqual(set(type(p) for p in passwords), {str})

    def test_hmac(self):
        os.environ['UD_HMAC_KEY'] = u'xxxxxxxx'
        self.assertEqual(
            userdir_ldap.ldap.make_passwd_hmac(b'status', b'purpose', b'uid', b'uuid', b'hosts', b'cryptedpass'),
            u'2e125f468a488cb73ce2fce6b76a7829878bf0d3')


if __name__ == '__main__':
    unittest.main()
